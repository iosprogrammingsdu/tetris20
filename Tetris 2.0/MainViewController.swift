//
//  MainViewController.swift
//  Tetris 2.0
//
//  Created by Adam Frémund on 21/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//

import Foundation
import CoreData
import UIKit

    
class MainViewController: UIViewController {
    private let userSettings = UserSettings.getInstance()
    private let highScore = HighScore.getInstance()
    
    @IBOutlet weak var highScoreLabel: UILabel!
    
    func retrieveData () {
        let bestPlayer = self.highScore.getBest()
        
        if bestPlayer == nil {
            self.highScoreLabel.text = "NO HIGHSCORE"
        } else {
            let playerName = bestPlayer!.name
            let playerScore = bestPlayer!.score
            
            highScoreLabel.text = "\(playerName.uppercased()) (\(playerScore))"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        retrieveData()
    }
    
    override func viewDidLoad() {
        if self.userSettings.playMusic() {
            AudioClass.getInstance().playBackgroundMusic()
        }
        
        super.viewDidLoad()
    }
}
