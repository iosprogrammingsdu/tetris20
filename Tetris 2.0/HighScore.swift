//
//  HighScore.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 27/11/2018.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation
import CoreData

class HighScore {
    private static var instance: HighScore?
    private static let TopScoreCount = 10
    
    private let topScoreCount: Int
    
    private init(topScoreCount: Int) {
        self.topScoreCount = topScoreCount
    }
    
    public func getAll() -> [Person] {
        let all = self.getCompleteAll()
        return all.count > self.topScoreCount ? Array(all[0..<self.topScoreCount]) : all
    }
    
    private func getCompleteAll() -> [Person] {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        let sort = NSSortDescriptor(key: "score", ascending: false)
        let sortDescriptors = [sort]
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let people = try PersistenceService.context.fetch(fetchRequest)
            return people
        } catch{
            return [Person]()
        }
    }
    
    public func getBest() -> Person? {
        let all = self.getAll()
        
        if all.count > 0 {
            return all.first
        }
        
        return nil
    }
    
    public func clear() {
        for person in self.getCompleteAll() {
            PersistenceService.context.delete(person)
            PersistenceService.saveContext()
        }
    }
    
    public func isTop(score: Int64) -> Bool {
        return self.getAll().count < self.topScoreCount || (self.getAll().last != nil && self.getAll().last!.score < score)
    }
    
    public func saveScore(playerName: String, score:Int64) {
        let person = Person(context: PersistenceService.context)
        person.name = playerName.uppercased()
        person.score = score
        PersistenceService.saveContext()
    }
    
    public static func getInstance() -> HighScore {
        if self.instance == nil {
            self.instance = HighScore(topScoreCount: HighScore.TopScoreCount)
        }
        
        return self.instance!
    }
}
