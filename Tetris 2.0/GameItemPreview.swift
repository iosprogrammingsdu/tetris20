//
//  GameItemPreview.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 11/28/18.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation
import SpriteKit

class GameItemPreview: SKScene {
    private var cols = 0
    private var rows = 0
    private var fields: [SKShapeNode] = []
    private var item: GameItem?
    private var refreshObservers: [RefreshGameProtocol] = []
    private var refreshRate: TimeInterval = 0.2
    
    init(size: CGSize, cols: Int, rows: Int) {
        super.init(size: size)
        self.cols = cols
        self.rows = rows
        
        self.backgroundColor = SKColor.black
        self.initPreview(cols: cols, rows: rows)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(renderScene), SKAction.wait(forDuration: self.refreshRate)])))
    }

    private func renderScene() {
        for col in 0..<self.cols {
            for row in 0..<self.rows {
                self.fields[col + row * self.cols].fillColor = SKColor.black
            }
        }
        
        if self.item == nil {
            return
        }
        
        var offset = 1
        
        if self.item!.cols == self.cols {
            offset = 0
        }
        
        for pos in self.item!.fields {
            if pos == nil {
                continue
            }
            
            self.fields[pos!.col + self.cols * pos!.row + offset].fillColor = self.item!.color
        }
    }
    
    private func initPreview(cols: Int, rows: Int) {
        let border = 0
        
        let fieldWidth = Int(self.size.width) - ((cols - 2) * border)
        let fieldHeight = Int(self.size.height) - ((rows - 2) * border)
        
        let itemWidth = fieldWidth / cols
        let itemHeight = fieldHeight / rows
        
        for row in 0...rows - 1 {
            for col in 0...cols - 1 {
                let item = SKShapeNode(rectOf: CGSize(width: itemWidth, height: itemHeight))
                item.lineWidth = 0
                item.fillColor = SKColor.black
                item.position = CGPoint(x: col * (itemWidth + border) + (itemWidth / 2), y: row * (itemHeight + border) + (itemHeight / 2))
                self.addChild(item)
                self.fields.append(item)
            }
        }
    }
    
    public func add(item: GameItem) {
        self.item = item
    }
}
