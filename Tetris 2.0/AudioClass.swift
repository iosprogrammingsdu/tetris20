//
//  AudioClass.swift
//  Tetris 2.0
//
//  Created by Pavel Novotný on 21/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//

import Foundation
import AVFoundation

public class AudioClass{
    private static var instance: AudioClass?
    
    private let pauseSoundFile = Bundle.main.path(forResource: "brick_sound.wav", ofType: nil)!
    private let lineSoundFile = Bundle.main.path(forResource: "line_sound.wav", ofType: nil)!
    private let gameEndSoundFile = Bundle.main.path(forResource: "game_over_sound.wav", ofType: nil)!
    private var audioplayer: AVAudioPlayer?
    private var pauseplayer: AVAudioPlayer?
    private var lineplayer: AVAudioPlayer?
    private var gameoverplayer: AVAudioPlayer?
    private let userSettings = UserSettings.getInstance()
    
    public static func getInstance() -> AudioClass {
        if self.instance == nil {
            self.instance = AudioClass()
        }
        
        return self.instance!
    }
    
    private init (){
        let path = Bundle.main.path(forResource: "background_sound.wav", ofType: nil)!
        do{
            audioplayer = try AVAudioPlayer(contentsOf:  URL(fileURLWithPath: path))
            pauseplayer = try AVAudioPlayer(contentsOf:  URL(fileURLWithPath: pauseSoundFile))
            lineplayer = try AVAudioPlayer(contentsOf:  URL(fileURLWithPath: lineSoundFile))
            gameoverplayer = try AVAudioPlayer(contentsOf:  URL(fileURLWithPath: gameEndSoundFile))
        } catch {
            print("Some of the audiofiles not found")
        }
    }
    
    public func playBackgroundMusic() {
        audioplayer?.numberOfLoops = -1
        audioplayer?.play()
    }
    
    public func stopBackgroundMusic() {
        audioplayer?.stop()
    }
    
    public func pauseSound(){
        if self.userSettings.playFxSound() {
            pauseplayer?.numberOfLoops = 0
            pauseplayer?.play()
        }
    }
    
    public func lineSound(){
        if self.userSettings.playFxSound() {
            lineplayer?.numberOfLoops = 0
            lineplayer?.play()
        }
    }
    
    public func  gameEndSound(){
        if self.userSettings.playFxSound() {
            gameoverplayer?.numberOfLoops = 0
            gameoverplayer?.play()
        }
    }
}
