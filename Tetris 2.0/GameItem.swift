//
//  GameItem.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 08/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//

import SpriteKit

class GameItem : CustomStringConvertible, Hashable {
    public var hashValue: Int {
        return self.color.hashValue + self.cols + self.rows + self.left + self.bottom + (self.isActive ? 1 : 0)
    }
    
    public let color: SKColor
    public var fields: [GamePosition?]
    public var cols: Int
    public var rows: Int
    public var left: Int
    public var bottom: Int
    public var isActive = true
    
    init(color: SKColor, cols: Int, rows: Int, fields: [GamePosition?]) {
        self.color = color
        self.cols = cols
        self.rows = rows
        self.fields = fields
        self.left = 0
        self.bottom = 0
    }
    
    public func rotate() {
        self.rotate(count: 1)
    }
    
    public func rotate(count: Int) {
        if count <= 0 {
            return
        }
        
        for _ in 1...count {
            var newFields = [GamePosition?](repeating: nil, count: self.cols * self.rows)
            
            for row in 0..<self.rows {
                for col in 0..<self.cols {
                    let position = self.get(col: col, row: row)
                    
                    if position != nil {
                        let newCol = row
                        let newRow = self.cols - col - 1
                        let newPosition = GamePosition(col: newCol, row: newRow)
                        let newIndex = self.rows * newRow + newCol
                        newFields[newIndex] = newPosition
                    }
                }
            }
            
            self.fields = newFields
            let oldCols = self.cols
            self.cols = self.rows
            self.rows = oldCols
        }
    }
    
    public func get(col: Int, row: Int) -> GamePosition? {
        let index = self.cols * row + col
        
        if col < 0 || row < 0 || col >= self.cols || row >= self.rows || !self.fields.indices.contains(index) {
            return nil
        }
        
        return self.fields[index]
    }
    
    public static func ==(first: GameItem, second: GameItem) -> Bool {
        if first.fields.count != second.fields.count {
            return false
        }
        
        for i in 0..<first.fields.count {
            if first.fields[i] != second.fields[i] {
                return false
            }
        }
        
        return first.bottom == second.bottom && first.left == second.left
    }
    
    public func removeRow(row: Int) {
        if row >= 0 && row < self.rows {
            var newFields: [GamePosition?] = []
            
            for i in 0..<self.fields.count {
                if i < row * self.cols || i >= (row + 1) * self.cols {
                    var field = self.fields[i]
                    
                    if field != nil && i >= (row + 1) * self.cols {
                        field?.row -= 1
                    }
                    
                    newFields.append(field)
                }
            }

            self.fields = newFields
            self.rows -= 1
        }
    }
    
    public var description: String {
        return "GameItem(cols: \(self.cols), height: \(self.rows), color: \(self.color))"
    }
}
