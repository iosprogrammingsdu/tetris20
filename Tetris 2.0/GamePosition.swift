//
//  GamePosition.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 08/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//p

struct GamePosition: Hashable {
    var col: Int;
    var row: Int;
    
    init(col: Int, row: Int) {
        self.col = col
        self.row = row
    }
    
    var hashValue: Int {
        get {
            return self.col.hashValue &* 31 &+ self.row.hashValue
        }
    }
    
    static func ==(first: GamePosition, second: GamePosition) -> Bool {
        return first.col == second.col && first.row == second.row
    }
}
