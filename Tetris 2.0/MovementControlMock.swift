//
//  MovementControlMock.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 26/11/2018.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation

class MovementControlMock : RefreshGameProtocol {
    private let conductor: GameConductor
    
    init(conductor: GameConductor) {
        self.conductor = conductor
    }
    
    func refreshScene() {
        // empty
    }
    
    func refreshGame() {
            let movement = Int(arc4random_uniform(3)) - 1
            if movement < 0 {
                self.conductor.moveLeft()
            } else if movement > 0 {
                self.conductor.moveRight()
            }
    }
}
