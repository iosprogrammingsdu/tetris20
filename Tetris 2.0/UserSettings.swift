//
//  Settings.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 28/11/2018.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class UserSettings {
    private static let KeyFxSound = "fx_sound"
    private static let KeyMusic = "music"
    private static let KeySensitivity = "sensitivity"
    
    private static var instance: UserSettings?
    
    private let storage = UserDefaults.standard
    
    private init() {
        if storage.object(forKey: UserSettings.KeyMusic) == nil {
            self.setPlayMusic(value: true)
            self.setPlayFxSound(value: true)
            self.setSensitivity(value: 5)
        }
    }
    
    public static func getInstance() -> UserSettings {
        if self.instance == nil {
            self.instance = UserSettings()
        }
        
        return self.instance!
    }
    
    public func playMusic() -> Bool {
        return self.storage.bool(forKey: UserSettings.KeyMusic)
    }
    
    public func setPlayMusic(value: Bool) {
        self.storage.setValue(value, forKey: UserSettings.KeyMusic)
    }
    
    public func playFxSound() -> Bool {
        return self.storage.bool(forKey: UserSettings.KeyFxSound)
    }
    
    public func setPlayFxSound(value: Bool) {
        self.storage.setValue(value, forKey: UserSettings.KeyFxSound)
    }
    
    public func sensitivity() -> Int {
        return self.storage.integer(forKey: UserSettings.KeySensitivity)
    }
    
    public func setSensitivity(value: Int) {
        self.storage.setValue(value, forKey: UserSettings.KeySensitivity)
    }
}
