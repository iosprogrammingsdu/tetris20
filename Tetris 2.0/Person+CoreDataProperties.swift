//
//  Person+CoreDataProperties.swift
//  Tetris 2.0
//
//  Created by Adam on 09/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var name: String
    @NSManaged public var score: Int64

}
