//
//  MovementControl.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 25/11/2018.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation
import CoreMotion

class MovementControl : RefreshGameProtocol {
    private static let threshold = 0.2
    
    private let motionManager = CMMotionManager()
    private let conductor: GameConductor
    private var initialized = false
    private let sensitivity: Double
    private var lastMovement: Date = Date()
    
    init(conductor: GameConductor, sensitivity: Double) {
        self.conductor = conductor
        self.sensitivity = sensitivity
        if self.motionManager.isAccelerometerAvailable {
            self.motionManager.startAccelerometerUpdates()
        }
    }
    
    public func pause() {
        self.motionManager.stopAccelerometerUpdates()
    }
    
    public func resume() {
        self.motionManager.startAccelerometerUpdates()
    }
    
    func refreshScene() {
        self.initialized = true
    }
    
    func refreshGame() {
        if self.shouldUpdate() && self.initialized, let data = self.motionManager.accelerometerData {
            self.lastMovement = Date()
            
            if data.acceleration.x < -MovementControl.threshold {
                self.conductor.moveLeft()
            } else if data.acceleration.x > MovementControl.threshold {
                self.conductor.moveRight()
            }
        }
    }
    
    private func shouldUpdate() -> Bool {
        let interval = Calendar.current.dateComponents([.nanosecond], from: self.lastMovement, to: Date()).nanosecond! / 1000000
        let target = (2.5 - self.sensitivity * 2) * 100
        
        return Double(interval) > target
    }
}
