//
//  TickProtocol.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 11/22/18.
//  Copyright © 2018 ZCU. All rights reserved.
//

import Foundation

protocol RefreshGameProtocol {
    func refreshScene()
    func refreshGame()
}
