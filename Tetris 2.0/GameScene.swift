//
//  GameScene.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 08/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    private var cols = 0
    private var rows = 0
    private var fields: [SKShapeNode] = []
    private var positions: [GameItem?] = []
    private var items: [GameItem] = []
    private var isStarted = false
    private var refreshObservers: [RefreshGameProtocol] = []
    public var refreshRate: TimeInterval = 0.2
    
    init(size: CGSize, cols: Int, rows: Int) {
        super.init(size: size)
        self.cols = cols
        self.rows = rows
        	
        self.backgroundColor = SKColor.black
        self.initGamefield(cols: cols, rows: rows)
    }
    
    public func addRefreshSceneObserver(observer: RefreshGameProtocol) {
        self.refreshObservers.append(observer)
    }
    
    override func didMove(to view: SKView) {
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(refreshScene), SKAction.wait(forDuration: self.refreshRate)])))
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(refreshGame), SKAction.wait(forDuration: 0.01)])))
    }
    
    private func refreshGame() {
        if !self.isStarted {
            return
        }
        
        for observer in self.refreshObservers {
            observer.refreshGame()
        }
    }
    
    public func start() {
        self.isStarted = true
    }
    
    public func stop() {
        self.isStarted = false
    }
    
    private func refreshScene() {
        if !self.isStarted {
            return
        }
        
        for observer in self.refreshObservers {
            observer.refreshScene()
        }
        
        self.renderScene()
    }
    
    private func renderScene() {
        for col in 0..<self.cols {
            for row in 0..<self.rows {
                self.fields[col + row * self.cols].fillColor = SKColor.black
                self.positions[col + row * self.cols] = nil
            }
        }
        
        for item in self.items {
            for pos in item.fields {
                if pos == nil {
                    continue
                }
                
                self.fields[item.left + pos!.col + self.cols * pos!.row + self.cols * item.bottom].fillColor = item.color
                self.positions[item.left + pos!.col + self.cols * pos!.row + self.cols * item.bottom] = item
            }
        }
    }
    
    private func initGamefield(cols: Int, rows: Int) {
        let border = 0
        
        let fieldWidth = Int(self.size.width) - ((cols - 2) * border)
        let fieldHeight = Int(self.size.height) - ((rows - 2) * border)
        
        let itemWidth = fieldWidth / cols
        let itemHeight = fieldHeight / rows
        
        for row in 0...rows - 1 {
            for col in 0...cols - 1 {
                let item = SKShapeNode(rectOf: CGSize(width: itemWidth, height: itemHeight))
                item.lineWidth = 0
                item.fillColor = SKColor.black
                item.position = CGPoint(x: col * (itemWidth + border) + (itemWidth / 2), y: row * (itemHeight + border) + (itemHeight / 2))
                self.addChild(item)
                self.fields.append(item)
                self.positions.append(nil)
            }
        }
    }
    
    public func add(item: GameItem) {
        self.items.append(item)
    }
    
    public func remove(item: GameItem) {
        let index = self.items.index{$0 === item}
        
        if index != nil {
            self.items.remove(at: index!)
            
            for i in 0..<self.positions.count {
                if self.positions[i] != nil && self.positions[i]! == item {
                    self.positions.remove(at: i)
                }
            }
        }
    }
    
    public func getPosition(col: Int, row: Int) -> GameItem? {
        return self.positions[col + row * self.cols]
    }
    
    public func setRefreshRate(refreshRate: TimeInterval) {
        self.refreshRate = refreshRate
    }
    
    public func triggerRender() {
        self.renderScene()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // not implemented
        super.init(coder: aDecoder)
    }
}
