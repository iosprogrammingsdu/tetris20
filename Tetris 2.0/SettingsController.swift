//
//  SettingsController.swift
//  Tetris 2.0
//
//  Created by Pavel Novotný on 22/11/2018.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SettingsController: UITableViewController {
    private let userSettings = UserSettings.getInstance()

    @IBOutlet weak var fxBtn: UISwitch!
    @IBOutlet weak var musicBtn: UISwitch!
    @IBOutlet weak var sensitivityLabel: UILabel!
    @IBOutlet weak var sensitivityStepper: UIStepper!
    
    override func viewDidLoad(){
        fxBtn.isOn = self.userSettings.playFxSound()
        musicBtn.isOn = self.userSettings.playMusic()
        self.sensitivityStepper.value = Double(self.userSettings.sensitivity())
        self.setSensitivityLabel()
        super.viewDidLoad()
    }
    
    @IBAction func music_control(_ sender: UISwitch) {
        self.userSettings.setPlayMusic(value: sender.isOn)
        
        if sender.isOn {
            AudioClass.getInstance().playBackgroundMusic()
        }
        else{
            AudioClass.getInstance().stopBackgroundMusic()
        }
    }
    
    @IBAction func fx_control(_ sender: UISwitch) {
        self.userSettings.setPlayFxSound(value: sender.isOn)
    }
    
    @IBAction func sensitivityStepperControl(_ sender: UIStepper) {
        let newValue = Int(sender.value)
        self.userSettings.setSensitivity(value: newValue)
        self.setSensitivityLabel()
    }

    private func setSensitivityLabel() {
        self.sensitivityLabel.text = "Sensitivity: \(self.userSettings.sensitivity())"
    }
}
