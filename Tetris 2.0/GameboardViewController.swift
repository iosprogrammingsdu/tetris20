//
//  GameboardViewController.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 24/10/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//

import UIKit
import SpriteKit

class GameboardViewController: UIViewController, GameStatus {
    private static let DefaultPlayerName = "Unknown"
    
    @IBOutlet weak var gameboardView: SKView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var itemPreviewView: SKView!
    private var gameConductor: GameConductor?
    private var gameItemPreview: GameItemPreview?
    private let highScore = HighScore.getInstance()
    private let audio = AudioClass.getInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        
        self.gameboardView.preferredFramesPerSecond = 120
        self.gameConductor = GameConductor(size: self.gameboardView.bounds.size)
        self.gameConductor?.addGameStatusListener(listener: self)
        let scene = self.gameConductor!.getGameScene()
        
        scene.scaleMode = .aspectFill
        self.gameboardView.presentScene(scene)
        self.gameboardView.isUserInteractionEnabled = true
        scene.isUserInteractionEnabled = true
        
        self.gameItemPreview = GameItemPreview(size: self.itemPreviewView.bounds.size, cols: 4, rows: 2)
        self.itemPreviewView.presentScene(self.gameItemPreview)
        
        self.gameConductor!.start()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
        super.viewWillDisappear(animated)
    }
    
    @IBAction func pauseClicked(_ sender: Any) {
        self.gameConductor?.pause()
        self.audio.pauseSound()
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Exit the game", style: .destructive) { _ in
            self.goToMainmenu()
        })
        
        alert.addAction(UIAlertAction(title: "Continue", style: .cancel) { _ in
            alert.dismiss(animated: true, completion: nil)
            self.gameConductor?.resume()
        })
        
        present(alert, animated: true)
    }
    
    private func goToMainmenu() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NavigationController")
        self.present(viewController, animated: false, completion: nil)
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let click = event?.touches(for: self.gameboardView)
        
        if click != nil && click!.count > 0 {
            self.gameConductor!.rotate()
        }
    }
    
    public func gameOver() {
        self.audio.gameEndSound()
        let score = Int64(self.scoreLabel.text!)!
        
        if self.highScore.isTop(score: score) {
            let alert = UIAlertController(title: "Score: \(score)" , message: "Enter your name", preferredStyle: .alert)
            alert.addTextField()
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let playerName = alert!.textFields![0]
                
                if playerName.text == nil || playerName.text!.isEmpty {
                    self.highScore.saveScore(playerName: GameboardViewController.DefaultPlayerName, score: score)
                }
                else {
                    self.highScore.saveScore(playerName: playerName.text!, score: score)
                }
                
                self.goToMainmenu()
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Game over", message: "You didn't make it to the high score. Try next time!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Go to menu", style: UIAlertActionStyle.default) {_ in self.goToMainmenu()})
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    public func score(change: Int) {
        self.scoreLabel.text = String(change)
    }
    
    public func next(item: GameItem) {
        self.gameItemPreview!.add(item: item)
    }
    
    public func lineClear() {
        self.audio.lineSound()
    }
}
