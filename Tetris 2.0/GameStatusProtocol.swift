//
//  GameStatusProtocol.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 25/11/2018.
//  Copyright © 2018 SDU. All rights reserved.
//

import Foundation

protocol GameStatus {
    func gameOver()
    func lineClear()
    func score(change: Int)
    func next(item: GameItem)
}
