//
//  HighScore.swift
//  Tetris 2.0
//
//  Created by Adam on 09/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//


import UIKit
import CoreData
class HighScoreViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    private let highScore = HighScore.getInstance()
    
    @IBAction func deleteData(){
        self.highScore.clear()
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.highScore.getAll().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = self.highScore.getAll()[indexPath.row].name
        cell.detailTextLabel?.text = String(self.highScore.getAll()[indexPath.row].score)
        return cell
    }
    
}
