//
//  GameConductor.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 11/22/18.
//  Copyright © 2018 ZCU. All rights reserved.
//

import Foundation
import SpriteKit

class GameConductor : RefreshGameProtocol {
    private static let DefaultCols = 10
    private static let DefaultRows = 20
    private static let ScoreIncrementLevel = 100
    private static let ScoreIncrementItem = 10
    
    private let userSettings = UserSettings.getInstance()
    private let gameScene: GameScene
    private let gameItemFactory: GameItemFactory
    private let size: CGSize
    private let cols: Int
    private let rows: Int
    private var items: [GameItem] = []
    private var isRunning = false
    private var gameStatusListeners: [GameStatus] = []
    private var nextItem: GameItem?
    private var score = 0
    private var movementControl: MovementControl?
    
    convenience init(size: CGSize) {
        self.init(size: size, cols: GameConductor.DefaultCols, rows: GameConductor.DefaultRows)
    }
    
    public func addGameStatusListener(listener: GameStatus) {
        self.gameStatusListeners.append(listener)
    }
    
    private func publishScore() {
        for listener in self.gameStatusListeners {
            listener.score(change: score)
        }
    }
    
    private func increaseScoreLevel() {
        self.score += GameConductor.ScoreIncrementLevel
        self.publishScore()
    }
    
    private func increaseScoreItem() {
        self.score += GameConductor.ScoreIncrementItem
        self.publishScore()
    }
    
    init(size: CGSize, cols: Int, rows: Int) {
        self.size = size
        self.cols = cols
        self.rows = rows
        self.gameScene = GameScene(size: self.size, cols: self.cols, rows: self.rows)
        self.gameScene.refreshRate = 0.5
        self.gameItemFactory = GameItemFactory()
    }
    
    public func start() {
        self.movementControl = MovementControl(conductor: self, sensitivity: Double(self.userSettings.sensitivity())/10.0)
        self.isRunning = true
        self.gameScene.addRefreshSceneObserver(observer: self)
        self.gameScene.addRefreshSceneObserver(observer: self.movementControl!)
        self.gameScene.start()
        self.addNewItem()
    }
    
    public func resume() {
        self.isRunning = true
        self.gameScene.start()
        self.movementControl?.resume()
    }
    
    public func pause() {
        self.isRunning = false
        self.gameScene.stop()
        self.movementControl?.pause()
    }
    
    public func stop() {
        self.pause()
    }
    
    public func getGameScene() -> GameScene {
        return self.gameScene
    }
    
    private func publishGameOver() {
        self.stop()
        
        for listener in self.gameStatusListeners {
            listener.gameOver()
        }
    }
    
    private func addNewItem() {
        let generatedActive = self.nextItem == nil ? self.generateItem() : nextItem!
        
        if !self.canSpawn(newItem: generatedActive) {
            self.publishGameOver()
            return
        }
        
        self.gameScene.add(item: generatedActive)
        self.items.append(generatedActive)
        self.increaseScoreItem()
        
        self.nextItem = self.generateItem()
        self.publishPreview()
    }
    
    private func publishLineClear() {
        for listener in self.gameStatusListeners {
            listener.lineClear()
        }
    }
    
    private func publishPreview() {
        if self.nextItem == nil {
            return
        }
        
        for listener in self.gameStatusListeners {
            listener.next(item: self.nextItem!)
        }
    }
    
    private func generateItem() -> GameItem {
        let item = self.gameItemFactory.create()
        item.bottom = self.getTop(for: item)
        item.left = self.getCenter(for: item)
        return item
    }
    
    private func getTop(for item : GameItem) -> Int {
        return self.rows - item.rows
    }
    
    private func getCenter(for item : GameItem) -> Int {
        return (self.cols / 2) - (item.cols / 2)
    }
    
    public func refreshScene() {
        if !self.isRunning {
            return
        }
        
        if !self.detectCollisions() {
            self.checkPoints()
            return
        }
        
        for item in self.items {
            if item.isActive {
                item.bottom -= 1
                break
            }
        }
    }
    
    public func refreshGame() {
        // empty
    }
    
    public func moveLeft() {
        for item in self.items {
            if item.isActive {
                if canMoveLeft(item: item) {
                    item.left -= 1
                    self.gameScene.triggerRender()
                }
                
                break
            }
        }
    }
    
    public func moveRight() {
        for item in self.items {
            if item.isActive {
                if canMoveRight(item: item) {
                    item.left += 1
                    self.gameScene.triggerRender()
                }
                
                break
            }
        }
    }
    
    public func rotate() {
        for item in self.items {
            if item.isActive {
                let copy = self.copy(item: item)
                copy.rotate()
                
                if self.canSpawnCopy(original: item, copy: copy) {
                    item.rotate()
                    self.gameScene.triggerRender()
                }
                
                break
            }
        }
    }
    
    private func detectCollisions() -> Bool  {
        for item in self.items {
            if item.isActive {
                if item.bottom == 0 {
                    item.isActive = false
                    self.addNewItem()
                    return false
                }
                
                for item2 in self.items {
                    if item == item2 {
                        continue
                    }
                    
                    if self.isStacked(active: item, on: item2) {
                        item.isActive = false
                        self.addNewItem()
                        return false
                    }
                }
            }
        }
        
        return true
    }
    
    private func isStacked(active: GameItem, on: GameItem) -> Bool {
        if(active.bottom > on.bottom + on.rows || active.bottom + active.rows < on.bottom
            || active.left + active.cols <= on.left || active.left >= on.left + on.cols) {
            return false
        }
        
        for field1 in active.fields {
            if field1 == nil || field1?.col == nil || field1?.row == nil {
                continue
            }
            
            let field1left = active.left + field1!.col
            let field1bottom = active.bottom + field1!.row
            
            for field2 in on.fields {
                if field2 == nil || field2?.col == nil || field2?.row == nil {
                    continue
                }
                
                let field2left = on.left + field2!.col
                let field2bottom = on.bottom + field2!.row
                
                if field1left == field2left && field1bottom == field2bottom + 1 {
                    return true
                }
            }
        }
        
        return false
    }
    
    private func canMoveLeft(item: GameItem) -> Bool {
        let copyLeft = self.copy(item: item)
        copyLeft.left -= 1
        return copyLeft.left >= 0 && self.canSpawnCopy(original: item, copy: copyLeft)
    }
    
    private func canMoveRight(item: GameItem) -> Bool {
        let copyRight = self.copy(item: item)
        copyRight.left += 1
        return copyRight.left + item.cols <= self.cols && self.canSpawnCopy(original: item, copy: copyRight)
    }
    
    private func canSpawnCopy(original: GameItem, copy: GameItem) -> Bool {
        var itemsCopy: [GameItem] = []
        
        for item in self.items {
            if item == original {
                continue
            }
            
            itemsCopy.append(item)
        }
        
        return self.canSpawn(newItem: copy, items: itemsCopy)
    }
    
    private func canSpawn(newItem: GameItem) -> Bool {
        return self.canSpawn(newItem: newItem, items: self.items)
    }
    
    private func canSpawn(newItem: GameItem, items: [GameItem]) -> Bool {
        for item in items {
            if self.isOverlap(item: item, item2: newItem) {
                return false
            }
        }
        
        return newItem.bottom + newItem.rows <= self.rows && newItem.left + newItem.cols <= self.cols
    }
    
    private func isOverlap(item: GameItem, item2: GameItem) -> Bool {
        if(item.bottom >= item2.bottom + item2.rows
            || item.bottom + item.rows <= item2.bottom
            || item.left + item.cols <= item2.left
            || item.left >= item2.left + item2.cols
            ) {
            return false
        }
        
        for field1 in item.fields {
            if field1 == nil || field1?.col == nil || field1?.row == nil {
                continue
            }
            
            let field1left = item.left + field1!.col
            let field1bottom = item.bottom + field1!.row
            
            for field2 in item2.fields {
                if field2 == nil || field2?.col == nil || field2?.row == nil {
                    continue
                }
                
                let field2left = item2.left + field2!.col
                let field2bottom = item2.bottom + field2!.row
                
                if field1left == field2left && field1bottom == field2bottom {
                    return true
                }
            }
        }
        
        return false
    }
    
    private func copy(item: GameItem) -> GameItem {
        let copy = GameItem(color: item.color, cols: item.cols, rows: item.rows, fields: [])
        
        for field in item.fields {
            if field == nil {
                copy.fields.append(nil)
            } else {
                copy.fields.append(GamePosition(col: field!.col, row: field!.row))
            }
        }
        
        copy.left = item.left
        copy.bottom = item.bottom
        
        return copy
    }
    
    private func checkPoints() {
        var runAgain = true
        
        while runAgain {
            runAgain = false
            
            for row in 0..<self.rows {
                var isFull = true
                var itemsToRemoveRow: Set = Set<GameItem>()
                
                for col in 0..<self.cols {
                    let item = self.gameScene.getPosition(col: col, row: row)
                    
                    if item == nil {
                        isFull = false
                        break
                    }
                    
                    itemsToRemoveRow.insert(item!)
                }
                
                if isFull {
                    for item in itemsToRemoveRow {
                        self.removeRowFrom(item: item, row: row)
                    }
                    
                    var itemsToLower: Set = Set<GameItem>()
                    
                    for upperRow in row+1..<self.rows {
                        for col in 0..<self.cols {
                            let loweredItem = self.gameScene.getPosition(col: col, row: upperRow)
                            if loweredItem != nil && loweredItem!.bottom >= upperRow {
                                itemsToLower.insert(loweredItem!)
                            }
                        }
                    }
                    
                    for itemToLower in itemsToLower {
                        itemToLower.bottom -= 1
                    }
                    
                    self.gameScene.triggerRender()
                    runAgain = true
                    self.publishLineClear()
                    self.increaseScoreLevel()
                    break
                }
                
                if runAgain {
                    break
                }
            }
        }
    }
    
    private func removeRowFrom(item: GameItem, row: Int) {
        for itemRow in 0..<item.rows {
            let globalRow = item.bottom + itemRow
            
            if globalRow == row {
                item.removeRow(row: itemRow)
                break
            }
        }
    }
}
			
