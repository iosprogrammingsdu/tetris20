//
//  GameItemFactory.swift
//  Tetris 2.0
//
//  Created by Martin Mach on 08/11/2018.
//  Copyright © 2018 ZCU. All rights reserved.
//

import SpriteKit

class GameItemFactory {
    private static let AllFields : [[GamePosition?]] = [
            [
                GamePosition(col: 0, row: 0),
                GamePosition(col: 1, row: 0),
                GamePosition(col: 2, row: 0),
                GamePosition(col: 3, row: 0)
            ],
            [
                GamePosition(col: 0, row: 0),
                GamePosition(col: 1, row: 0),
                GamePosition(col: 2, row: 0),
                GamePosition(col: 0, row: 1),
                nil,
                nil
            ],
            [
                GamePosition(col: 0, row: 0),
                GamePosition(col: 1, row: 0),
                GamePosition(col: 2, row: 0),
                nil,
                nil,
                GamePosition(col: 2, row: 1)
            ],
            [
                GamePosition(col: 0, row: 0),
                GamePosition(col: 1, row: 0),
                GamePosition(col: 0, row: 1),
                GamePosition(col: 1, row: 1)
            ],
            [
                GamePosition(col: 0, row: 0),
                GamePosition(col: 1, row: 0),
                nil,
                nil,
                GamePosition(col: 1, row: 1),
                GamePosition(col: 2, row: 1)
            ],
            [
                GamePosition(col: 0, row: 0),
                GamePosition(col: 1, row: 0),
                GamePosition(col: 2, row: 0),
                nil,
                GamePosition(col: 1, row: 1),
                nil
            ],
            [
                nil,
                GamePosition(col: 1, row: 0),
                GamePosition(col: 2, row: 0),
                GamePosition(col: 0, row: 1),
                GamePosition(col: 1, row: 1),
                nil
            ]
        ]
    
    private static let Colors = [SKColor.cyan, SKColor.blue, SKColor.orange, SKColor.yellow, SKColor.green, SKColor.purple, SKColor.red]
    
    public func create() -> GameItem {
        let rand = Int(arc4random_uniform(UInt32(GameItemFactory.AllFields.count)))
        let fields = GameItemFactory.AllFields[rand]
        let color = GameItemFactory.Colors[rand]
        let dim = self.getDimensions(positions: fields)
        let item = GameItem(color: color, cols: dim.0, rows: dim.1, fields: fields)
        // let rotate = Int(arc4random_uniform(4))
        // item.rotate(count: rotate)
        return item
    }
    
    private func getDimensions(positions: [GamePosition?]) -> (Int, Int) {
        var cols = 0
        var rows = 0
        
        for pos in positions {
            if pos == nil {
                continue
            }
            
            if pos!.col > cols {
                cols = pos!.col
            }
            
            if pos!.row > rows {
                rows = pos!.row
            }
        }
        
        return (cols + 1, rows + 1)
    }
}
